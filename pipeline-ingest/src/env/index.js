const dotenv = require('dotenv').config().parsed;

module.exports = {
    ...dotenv,
    ...process.env
};
