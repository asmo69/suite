cd api-graphql
npm run build

cd ../api-rest-account
npm run build

cd ../api-rest-document
npm run build

cd ../api-rest-project
npm run build

cd ../api-rest-session
npm run build



cd ../pipeline-fakespeare
npm run build

cd ../pipeline-import
npm run build

cd ../pipeline-newsapi
npm run build

cd ../pipeline-webhose
npm run build



cd ../pipeline-ingest
npm run build

cd ../pipeline-translate
npm run build

cd ../pipeline-spacy
npm run build

cd ../pipeline-markup
npm run build



cd ../pipeline-file
npm run build

cd ../pipeline-mysql
npm run build



cd ../web-proxy
npm run build

cd ../web-account
npm run build

cd ../web-feed
npm run build

cd ../web-home
npm run build

cd ../web-project
npm run build



docker-compose build
