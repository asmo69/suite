module.exports = (text, size) => {
    let sentences = text.split(/(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s/gm);
    let blocks = [];
    let block = '';

    for (let x = 0; x < sentences.length; x++) {
        if (block.length + sentences[x].length < size) {
            block += sentences[x];
        } else {
            blocks.push(block);
            block = sentences[x];
        }
    }
    if (block) {
        blocks.push(block);
    }

    return blocks;
};
