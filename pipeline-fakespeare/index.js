const fs = require('fs');

const pkg = require('./package.json');
const env = require('./src/env');
const split = require('./src/split');
const Kafka = require('./src/kafka');

const topics = {
    produce: env.KAFKA_PRODUCE || null,
    consume: env.KAFKA_CONSUME || null
};

const app = new Kafka();

const main = async () => {
    const docid =
        'document/fakespeare/' +
        Date.now() +
        '-' +
        Math.ceil(Math.random() * 999999);

    const pages = split(
        fs.readFileSync('./assets/romeo-and-juliet.txt', 'utf-8'),
        10240
    );

    app.connect({
        clientId: pkg.name,
        brokers: [env.KAFKA_HOST || '127.0.0.1:9092']
    });

    for (let x = 0; x < pages.length; x++) {
        await app.send(topics.produce, {
            uuid: docid,

            source: 'fakespeare',
            subsource: '',
            mime: 'text/plain',
            language: 'en',
            published: new Date(),
            created: new Date(),
            updated: new Date(),
            pages: pages.length,

            url: '',
            title: 'Romeo and Juliet',
            image: '',

            page: {
                text: pages[x],
                page: x + 1,
                language: 'en'
            }
        });
    }

    app.disconnect();
};

main().catch((error) => {
    console.error(`💣 Error... %s`, error.message || error);
    app.disconnect();
    process.exit(0);
});
