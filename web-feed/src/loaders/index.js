import feed from '../redux/actions/feed';

import whoami from '../graphql/whoami';
import document from '../graphql/document';
import entity from '../graphql/entity';
import none from '../graphql/none';

const loader = ({store, request, params, client}) => {
    const {query, cookies, headers} = request;
    const authorization = headers.authorization || cookies.authorization || '';

    const selected = Array.isArray(query.selected)
        ? query.selected
        : (query.selected || '0').split(',');

    const data = {
        user: authorization ? whoami() : none(),
        document: params.hash
            ? document({
                args: {
                    uuid: `document/${params.source}/${params.hash}`,
                    language: params.language || 'en'
                },
                include: {
                    entity: true,
                    page: true,
                    meta: true
                }
            })
            : none(),
        documents: query.selected
            ? document({
                args: {
                    all_entity_id: selected.map(item => parseInt(item, 10)),
                    language: 'en',
                    order: 'title'
                }
            })
            : document({
                args: {
                    language: 'en',
                    order: 'title'
                }
            }),
        selected: query.selected
            ? entity({
                args: {
                    id: selected.map(item => parseInt(item, 10)),
                    order: 'name'
                }
            })
            : none(),
        entities: query.search
            ? entity({
                args: {
                    name: `%${query.search}%`,
                    order: 'name'
                }
            })
            : none()
    };

    return feed(client, data)(store.dispatch);
};

export default loader;
