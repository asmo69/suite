import dotenv from 'dotenv';

const env = () => {
    const local = dotenv.config().parsed;

    return {
        ...local,
        ...process.env
    };
};

export default env();
