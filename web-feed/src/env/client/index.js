import jwt from 'jwt-simple';

const env = () => {
    let retval = {};

    try {
        retval = JSON.parse(jwt.decode(window.INITIAL_ENV, NODE_SECRET_KEY));
    } catch (error) {
        retval = {};
    }

    return retval;
};

export default env();
