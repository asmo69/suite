import {gql} from '@apollo/client';
import {jsonToGraphQLQuery} from 'json-to-graphql-query';

import * as constants from '../../constants';

export default (client, data) => dispatch => {
    dispatch({
        type: constants.GET_GRAPHQL
    });

    const query = jsonToGraphQLQuery({query: data}, {pretty: true});

    console.log('--------------------------------------');
    console.log(query);
    console.log('--------------------------------------');

    return client
        .query({
            query: gql(query)
        })
        .then(response => {
            dispatch({
                type: constants.GET_GRAPHQL_SUCCESS,
                payload: response
            });
        })
        .catch(error => {
            dispatch({
                type: constants.GET_GRAPHQL_FAIL,
                payload: error
            });
        });
};
