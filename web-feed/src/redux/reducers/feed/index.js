import * as constants from '../../constants';

const initial = {
    loading: false,
    error: false,
    data: {},
    search: {}
};

export default (state = initial, action) => {
    if (action.type === constants.GET_GRAPHQL) {
        return {
            ...state,
            loading: true,
            error: false
        };
    }
    if (action.type === constants.GET_GRAPHQL_SUCCESS) {
        return {
            ...state,
            loading: false,
            error: false,
            data: {
                ...state.data,
                ...action.payload.data
            }
        };
    }
    if (action.type === constants.GET_GRAPHQL_FAIL) {
        return {
            ...state,
            loading: false,
            error: true,
            data: null
        };
    }

    return state;
};
