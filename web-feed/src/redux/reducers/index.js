import feed from './feed';

const reducers = {
    feed,
    default: (state = {}) => state
};

export default reducers;
