import Feed from '../../pages/feed';
import View from '../../pages/view';
import Home from '../../pages/home';

import loader from '../../loaders';

export default [
    {
        path: '/feed',
        component: Feed,
        exact: true,
        loader
    },
    {
        path: '/feed/document/:source/:hash',
        component: View,
        exact: true,
        loader
    },
    {
        path: '/feed/document/:source/:hash/:lang',
        component: View,
        exact: true,
        loader
    },
    {
        path: '/',
        component: Home,
        exact: true
    }
];
