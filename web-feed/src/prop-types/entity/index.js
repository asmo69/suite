import PropTypes from 'prop-types';

export default PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    type: PropTypes.string,
    text: PropTypes.string
});
