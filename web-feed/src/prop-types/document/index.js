import PropTypes from 'prop-types';

export default PropTypes.shape({
    id: PropTypes.number,
    uuid: PropTypes.string,
    language: PropTypes.string,
    source: PropTypes.string,
    subsource: PropTypes.string,
    mime: PropTypes.string,
    published: PropTypes.string,
    created: PropTypes.string,
    updated: PropTypes.string,
    pages: PropTypes.number,
    url: PropTypes.string,
    title: PropTypes.string,
    image: PropTypes.string,
    page: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number,
            page: PropTypes.number,
            language: PropTypes.string,
            text: PropTypes.string,
            markup: PropTypes.string
        })
    )
});
