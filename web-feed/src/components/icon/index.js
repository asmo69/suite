import React from 'react';
import PropTypes from 'prop-types';

import Styles from './style.scss';

const icons = {
    DATE: <i className="fas fa-calendar-alt" />,
    GPE: <i className="fas fa-map-marker-alt" />,
    PERSON: <i className="fas fa-user" />,
    ORG: <i className="fas fa-building" />,
    CARDINAL: <i className="fas fa-hashtag" />,
    EVENT: <i className="fas fa-question" />,
    FAC: <i className="fas fa-industry" />,
    LAW: <i className="fas fa-gavel" />,
    LOC: <i className="fas fa-question" />,
    MONEY: <i className="fas fa-money-bill-alt" />,
    NORP: <i className="fas fa-question" />,
    ORDINAL: <i className="fas fa-hashtag" />,
    PERCENT: <i className="fas fa-percent" />,
    PRODUCT: <i className="fas fa-box-open" />,
    QUANTITY: <i className="fas fa-question" />,
    TIME: <i className="fas fa-clock" />,
    WORK_OF_ART: <i className="fas fa-palette" />,
    UNKNOWN: <i className="fas fa-question" />
};

const Icon = ({type}) => {
    return <span className={Styles[type]}>{icons[type] || icons.UNKNOWN}</span>;
};

Icon.propTypes = {
    type: PropTypes.string.isRequired
};

export default Icon;
