import React from 'react';
import PropTypes from 'prop-types';

const Main = ({children}) => {
    return <div>{children}</div>;
};

Main.propTypes = {
    children: PropTypes.any
};

Main.defaultProps = {
    children: []
};

export default Main;
