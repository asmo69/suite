import React from 'react';
import PropTypes from 'prop-types';

import Card from '..';
import Markup from '../../markup';

import DocumentType from '../../../prop-types/document';

const DocumentDetails = ({data}) => {
    if (data.length > 0) {
        return (
            <Card>
                <h2>{data[0].title}</h2>
                <Markup text={data[0].page[0].markup} />
            </Card>
        );
    }
    return null;
};

DocumentDetails.propTypes = {
    data: PropTypes.arrayOf(DocumentType)
};

DocumentDetails.defaultProps = {
    data: []
};

export default DocumentDetails;
