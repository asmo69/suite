import React from 'react';
import {Link} from 'react-router-dom';

import {Row, Col} from 'reactstrap';
import Card from '..';

import useURL from '../../../hooks/use-url';

import EntityType from '../../../prop-types/entity';

import Icon from '../../icon';

const EntitySummary = ({data}) => {
    const url = useURL();

    const selected = Array.isArray(url.query.selected)
        ? url.query.selected
        : (url.query.selected || '0').split(',');

    if (selected.indexOf(data.id.toString()) === -1) {
        url.query.selected = [...selected, data.id].join(',');
    }
    url.query.search = '';

    return (
        <Card>
            <Row>
                <Col xs={2}>
                    <Icon type={data.type} />
                </Col>
                <Col xs={8}>
                    <Link to={url.toString()}>{data.name}</Link>
                    <ul>
                        <li>{data.type}</li>
                    </ul>
                </Col>
                <Col xs={2}>
                    <i className="fas fa-ellipsis-v" />
                </Col>
            </Row>
        </Card>
    );
};

EntitySummary.propTypes = {
    data: EntityType
};

EntitySummary.defaultProps = {
    data: {}
};

export default EntitySummary;
