import React from 'react';
import {Link} from 'react-router-dom';

import {Row, Col} from 'reactstrap';
import Card from '..';

import useURL from '../../../hooks/use-url';

import DocumentType from '../../../prop-types/document';

const DocumentSummary = ({data}) => {
    const url = useURL();

    url.pathname = `/feed/${data.uuid}`;

    const image = () => {
        if (data.image) {
            <img src={data.image} width={32} alt="[IMAGE}" />;
        }
    };

    return (
        <Card>
            <Row>
                <Col xs={1}>{image()}</Col>
                <Col xs={10}>
                    <Link to={url.toString()}>{data.title}</Link>
                    <ul>
                        <li>{data.subsource}</li>
                        <li>{data.published}</li>
                    </ul>
                </Col>
                <Col xs={1}>
                    <i className="fas fa-ellipsis-v" />
                </Col>
            </Row>
        </Card>
    );
};

DocumentSummary.propTypes = {
    data: DocumentType
};

DocumentSummary.defaultProps = {
    data: {}
};

export default DocumentSummary;
