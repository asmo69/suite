import React from 'react';
import PropTypes from 'prop-types';

import {Card, CardBody} from 'reactstrap';

import Styles from './style.scss';

const CustomCard = ({children}) => {
    return (
        <Card className={Styles.Card}>
            <CardBody>{children}</CardBody>
        </Card>
    );
};

CustomCard.propTypes = {
    children: PropTypes.any
};

CustomCard.defaultProps = {
    children: []
};

export default CustomCard;
