import React from 'react';
import PropTypes from 'prop-types';
import {useHistory} from 'react-router-dom';

import {Form, InputGroup, InputGroupAddon, Input, Button} from 'reactstrap';
import CardsListPanel from '../card-list';

import useURL from '../../../hooks/use-url';

import Styles from './style.scss';

const SearchPanel = ({results, component, title}) => {
    const url = useURL();
    const history = useHistory();

    const hidden = () =>
        Object.keys(url.query)
            .filter(key => key !== 'search')
            .map((key, index) => (
                <input
                    key={index}
                    name={key}
                    type="hidden"
                    value={url.query[key]}
                />
            ));

    const resultsList = () => {
        if (results.length) {
            return (
                <CardsListPanel
                    className={Styles.ResultsList}
                    results={results}
                    component={component}
                />
            );
        }
        return null;
    };

    const handleChange = () => e => {
        url.query.search = e.target.value;

        history.push(url.toString());
    };

    const form = () => {
        return (
            <Form method="get" action="">
                {hidden()}
                <InputGroup>
                    <Input
                        name="search"
                        autoComplete="off"
                        placeholder="Search for an Entity"
                        defaultValue={url.query.search}
                        onChange={handleChange()}
                    />
                    <InputGroupAddon addonType="append">
                        <Button>
                            <i className="fas fa-search" />
                        </Button>
                    </InputGroupAddon>
                </InputGroup>
            </Form>
        );
    };

    const classes = results.length ? Styles.Open : Styles.Closed;

    return (
        <div className={Styles.SearchPanel}>
            {title && <h2>{title}</h2>}
            {form()}
            <div className={classes}>{resultsList()}</div>
        </div>
    );
};

SearchPanel.propTypes = {
    title: PropTypes.string,
    results: PropTypes.array,
    component: PropTypes.func
};

SearchPanel.defaultProps = {
    title: '',
    results: [],
    component: () => null
};

export default SearchPanel;
