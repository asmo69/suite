import React from 'react';
import PropTypes from 'prop-types';

import classnames from 'classnames';

import Styles from './style.scss';

const SlideRightPanel = ({children}) => {
    return (
        <div className={classnames('slide-right', Styles.SlideRightPanel)}>
            {children}
        </div>
    );
};

SlideRightPanel.propTypes = {
    children: PropTypes.any
};

SlideRightPanel.defaultProps = {
    children: []
};

export default SlideRightPanel;
