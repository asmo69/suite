import React from 'react';
import PropTypes from 'prop-types';

import classnames from 'classnames';

import Styles from './style.scss';

const SlidePanel = ({children, className, isOpen}) => {
    const classes = classnames(
        Styles.SlidePanel,
        isOpen ? 'slide-open' : 'slide-closed',
        className,
        Styles.SlideLeft
    );

    return <div className={classes}>{children}</div>;
};

SlidePanel.propTypes = {
    className: PropTypes.string,
    isOpen: PropTypes.bool,
    children: PropTypes.any
};

SlidePanel.defaultProps = {
    className: null,
    isOpen: true,
    children: []
};

export default SlidePanel;
