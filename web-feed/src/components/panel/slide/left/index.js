import React from 'react';
import PropTypes from 'prop-types';

import classnames from 'classnames';

import Styles from './style.scss';

const SlideLeftPanel = ({children}) => {
    return (
        <div className={classnames('slide-left', Styles.SlideLeftPanel)}>
            {children}
        </div>
    );
};

SlideLeftPanel.propTypes = {
    children: PropTypes.any
};

SlideLeftPanel.defaultProps = {
    children: []
};

export default SlideLeftPanel;
