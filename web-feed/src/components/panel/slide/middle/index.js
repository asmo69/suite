import React from 'react';
import PropTypes from 'prop-types';

import classnames from 'classnames';

import Styles from './style.scss';

const SlideMiddlePanel = ({children}) => {
    return (
        <div className={classnames('slide-middle', Styles.SlideMiddlePanel)}>
            <div
                className={classnames('slide-toggle', Styles.SlideTogglePanel)}
            >
                {children}
            </div>
        </div>
    );
};

SlideMiddlePanel.propTypes = {
    children: PropTypes.any
};

SlideMiddlePanel.defaultProps = {
    children: []
};

export default SlideMiddlePanel;
