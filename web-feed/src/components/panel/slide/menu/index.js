import React from 'react';
import PropTypes from 'prop-types';

import classnames from 'classnames';

import Styles from './style.scss';

const SlideMenuPanel = ({children}) => {
    return (
        <div className={classnames('slide-menu', Styles.SlideMenuPanel)}>
            {children}
        </div>
    );
};

SlideMenuPanel.propTypes = {
    children: PropTypes.any
};

SlideMenuPanel.defaultProps = {
    children: []
};

export default SlideMenuPanel;
