import React from 'react';
import PropTypes from 'prop-types';

import classnames from 'classnames';

import Styles from './style.scss';

const CardsListPanel = ({className, results, component}) => {
    const list = results.map((item, index) =>
        React.createElement(component, {
            key: item.uuid || item.id || index,
            data: item
        })
    );

    const classes = classnames(className, Styles.CardListPanel);

    return <div className={classes}>{list}</div>;
};

CardsListPanel.propTypes = {
    className: PropTypes.string,
    results: PropTypes.array,
    component: PropTypes.func
};

CardsListPanel.defaultProps = {
    className: null,
    results: [],
    component: () => null
};

export default CardsListPanel;
