import React from 'react';
import {Link} from 'react-router-dom';

import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';

import useURL from '../../hooks/use-url';

import Logo from './logo.svg';
import Styles from './style.scss';

const Header = () => {
    const currentURL = useURL();
    const homeURL = useURL();
    const feedURL = useURL();
    const projectURL = useURL();

    const {menu} = currentURL.query;

    currentURL.query.menu = currentURL.query.menu === 'true' ? 'false' : 'true';
    homeURL.pathname = '/';
    feedURL.pathname = '/feed';
    projectURL.pathname = '/project';

    return (
        <div>
            <Navbar color="light" light expand="md" className={Styles.Header}>
                <NavbarBrand tag={Link} to={homeURL.toString()}>
                    <img src={Logo} alt="[Adarga]" width={32} />
                    Adarga Suite
                </NavbarBrand>
                <NavbarToggler tag={Link} to={currentURL.toString()} />
                <Collapse isOpen={menu === 'true'} navbar>
                    <Nav className="mr-auto" navbar>
                        <NavItem>
                            <NavLink tag={Link} to={homeURL.toString()}>
                                Home
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={Link} to={feedURL.toString()}>
                                Feed
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={Link} to={projectURL.toString()}>
                                Project
                            </NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        </div>
    );
};

export default Header;
