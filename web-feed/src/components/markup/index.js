import React from 'react';
import PropTypes from 'prop-types';

import Styles from './style.scss';

const Markup = ({text}) => {
    return (
        <div
            className={Styles.Markup}
            dangerouslySetInnerHTML={{
                __html: text
            }}
        />
    );
};

Markup.propTypes = {
    text: PropTypes.string
};

Markup.defaultProps = {
    text: ''
};

export default Markup;
