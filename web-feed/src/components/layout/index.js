import React from 'react';
import PropTypes from 'prop-types';

import {Container} from 'reactstrap';

import Header from '../header';
import Main from '../main';
import Footer from '../footer';

const Layout = ({children}) => {
    return (
        <>
            <Container fluid className="p-0">
                <Header />
            </Container>
            <Container fluid>
                <Main>{children}</Main>
            </Container>
            <Container fluid className="p-0">
                <Footer />
            </Container>
        </>
    );
};

Layout.propTypes = {
    children: PropTypes.any
};

Layout.defaultProps = {
    children: []
};

export default Layout;
