import React from 'react';

import Styles from './style.scss';

const Footer = () => {
    return <div className={Styles.Footer}>Adarga &copy; 2020</div>;
};

export default Footer;
