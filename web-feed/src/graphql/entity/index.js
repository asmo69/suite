export default (props = {}) => {
    const {args} = props;

    const retval = {
        __aliasFor: 'entity',
        __args: args,
        id: true,
        name: true,
        type: true,
        text: true
    };

    return retval;
};
