export default (props = {}) => {
    const {args} = props;
    return {
        __aliasFor: 'whoami',
        __args: args,
        email: true,
        name: true
    };
};
