export default (props = {}) => {
    const {args, include} = props;

    const retval = {
        __aliasFor: 'document',
        __args: args,
        id: true,
        uuid: true,
        language: true,
        source: true,
        subsource: true,
        mime: true,
        published: true,
        created: true,
        updated: true,
        pages: true,
        url: true,
        title: true,
        image: true
    };

    if (include) {
        if (include.page) {
            retval.page = {
                __aliasFor: 'page',
                __args: {
                    order: 'page'
                },
                id: true,
                language: true,
                page: true,
                text: true,
                markup: true
            };
        }
        if (include.meta) {
            retval.meta = {
                __aliasFor: 'meta',
                __args: {
                    order: 'name'
                },
                id: true,
                name: true,
                value: true
            };
        }
        if (include.entity) {
            retval.entity = {
                __aliasFor: 'entity',
                __args: {
                    order: 'name'
                },
                id: true,
                name: true,
                type: true,
                text: true
            };
        }
    }

    return retval;
};
