import React from 'react';

import {Row, Col} from 'reactstrap';

import Layout from '../../components/layout';

const Home = () => {
    return (
        <Layout>
            <Row>
                <Col xs={12}>This page should be hidden.</Col>
            </Row>
        </Layout>
    );
};

export default Home;
