import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {Link} from 'react-router-dom';

import {Badge, Row, Col} from 'reactstrap';

import Layout from '../../components/layout';
import SlidePanel from '../../components/panel/slide';
import SlideMenuPanel from '../../components/panel/slide/menu';
import SlideLeftPanel from '../../components/panel/slide/left';
import SlideMiddlePanel from '../../components/panel/slide/middle';
import SlideRightPanel from '../../components/panel/slide/right';

import CardsListPanel from '../../components/panel/card-list';

import DocumentDetailsCard from '../../components/card/document-details';
import EntitySummaryCard from '../../components/card/entity-summary';

import useURL from '../../hooks/use-url';

// import Styles from '../../styles/style.scss';

const View = ({document}) => {
    const currentURL = useURL();

    const side = currentURL.query.side === 'true';

    const menu = () => {
        const searchURL = useURL();
        const filtersURL = useURL();
        const settingsURL = useURL();

        searchURL.query.side = 'true';
        searchURL.query.section = 'search';

        filtersURL.query.side = 'true';
        filtersURL.query.section = 'filters';
        filtersURL.query.search = '';

        settingsURL.query.side = 'true';
        settingsURL.query.section = 'settings';
        settingsURL.query.search = '';

        return (
            <Row>
                <Col>
                    <Link to={searchURL.toString()}>
                        <i className="fas fa-search" />
                    </Link>
                </Col>
                <Col>
                    <Link to={filtersURL.toString()}>
                        <i className="fas fa-filter" />
                    </Link>
                </Col>
                <Col>
                    <Link to={settingsURL.toString()}>
                        <i className="fas fa-cog" />
                    </Link>
                </Col>
            </Row>
        );
    };

    const left = () => {
        if (side) {
            return (
                <>
                    <CardsListPanel
                        results={document[0].entity}
                        component={EntitySummaryCard}
                    />
                </>
            );
        }
        return null;
    };

    const middle = () => {
        const url = useURL();

        if (side) {
            url.query.side = 'false';
            url.query.search = '';
            return (
                <Badge tag={Link} to={url.toString()} pill>
                    <i className="fas fa-arrow-left" />
                </Badge>
            );
        }
        url.query.side = 'true';
        url.query.search = '';
        return (
            <Badge tag={Link} to={url.toString()} pill>
                <i className="fas fa-arrow-right" />
            </Badge>
        );
    };

    const right = () => {
        return (
            <>
                <DocumentDetailsCard data={document} />
            </>
        );
    };

    return (
        <Layout>
            <SlidePanel isOpen={side}>
                <SlideMenuPanel>{menu()}</SlideMenuPanel>
                <SlideLeftPanel>{left()}</SlideLeftPanel>
                <SlideMiddlePanel>{middle()}</SlideMiddlePanel>
                <SlideRightPanel>{right()}</SlideRightPanel>
            </SlidePanel>
        </Layout>
    );
};

View.propTypes = {
    document: PropTypes.array
};

View.defaultProps = {
    document: []
};

const mapStateToProps = state => {
    const data = state && state.feed && state.feed.data ? state.feed.data : {};

    return {
        loading: state.feed.loading,
        error: state.feed.error,
        document: data.document
    };
};

export default connect(mapStateToProps)(View);
