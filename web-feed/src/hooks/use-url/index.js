import {useLocation} from 'react-router-dom';
import queryString from 'query-string';

export default (pathname, query = {}) => {
    const location = useLocation();

    const url = {
        pathname: pathname || location.pathname,
        query: {...queryString.parse(location.search), ...query},
        hash: location.hash
    };

    url.toString = () => {
        let result = '';
        const qs = queryString.stringify(url.query);

        result += url.pathname;
        if (qs) {
            result += `?${qs}`;
        }
        if (url.hash) {
            result += `${url.hash}`;
        }

        return result;
    };

    return url;
};
