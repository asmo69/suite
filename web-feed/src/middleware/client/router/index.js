import React from 'react';
import ReactDOM from 'react-dom';

import {BrowserRouter, Switch, Route} from 'react-router-dom';

import {Provider} from 'react-redux';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

import {
    ApolloClient,
    InMemoryCache,
    ApolloProvider,
    HttpLink
} from '@apollo/client';

import fetch from 'isomorphic-fetch';
import https from 'https';

import jwt from 'jwt-simple';

import reducers from '../../../redux/reducers';

export default (env, routes) => {
    console.log('router...');

    let state = {};
    try {
        state = JSON.parse(jwt.decode(window.INITIAL_STATE, NODE_SECRET_KEY));
    } catch (error) {
        state = {};
    }

    const store = createStore(
        combineReducers(reducers),
        state,
        applyMiddleware(thunk)
    );

    const client = new ApolloClient({
        link: new HttpLink({
            uri: 'https://localhost:5000/graphql',
            fetch,
            fetchOptions: {
                agent: new https.Agent({
                    rejectUnauthorized: false
                })
            }
        }),
        cache: new InMemoryCache()
    });

    const routing = routes.map((route, index) => {
        return (
            <Route
                key={index}
                path={route.path}
                component={route.component}
                exact={route.exact}
            />
        );
    });

    const App = () => {
        return (
            <Provider store={store}>
                <ApolloProvider client={client}>
                    <BrowserRouter>
                        <Switch>{routing}</Switch>
                    </BrowserRouter>
                </ApolloProvider>
            </Provider>
        );
    };

    ReactDOM.hydrate(<App />, document.getElementById('app'));
};
