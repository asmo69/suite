import assets from './assets';
import compression from './compression';
import common from './common';
import morgan from './morgan';
import handlebars from './handlebars';
import statics from './statics';
import router from './router';
import express from './express';

export default [
    assets,
    compression,
    common,
    morgan,
    handlebars,
    statics,
    router,
    express
];
