/* eslint-disable */

import fs from 'fs';
import path from 'path';
import jwt from 'jwt-simple';

import React from 'react';

import { renderToString } from 'react-dom/server';
import { StaticRouter, Switch, Route } from 'react-router-dom';
import { matchRoutes } from 'react-router-config';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import { ApolloClient, InMemoryCache, ApolloProvider, HttpLink } from '@apollo/client';

import fetch from 'isomorphic-fetch';
import https from 'https';

import reducers from '../../../redux/reducers';

export default (app, config, routes) => {
    console.log('router...');

    app.use((req, res) => {
        const state = {};
        const store = createStore(
            combineReducers(reducers),
            state,
            applyMiddleware(thunk)
        );

        const client = new ApolloClient({
            link: new HttpLink({
                uri: 'https://localhost:5000/graphql', 
                fetch, 
                fetchOptions: {
                    agent: new https.Agent({ 
                        rejectUnauthorized: false
                    })
                }
            }),
            cache: new InMemoryCache()
        });

        const matches = matchRoutes(routes, req._parsedUrl.pathname);
        const promises = matches.map((match) => {
            const loader = match.route.loader ? match.route.loader : null;

            return loader
                ? loader({
                    request: req,
                    response: res,
                    store,
                    match,
                    client,
                    params: Object.assign({}, match.match.params)
                })
                : Promise.resolve(null);
        });

        Promise.all(promises).then(() => {
            const routing = routes.map((route, index) => {
                return (
                    <Route
                        key={index}
                        path={route.path}
                        component={route.component}
                        exact={route.exact}
                    />
                );
            });

            const content = renderToString(
                <Provider store={store}>
                    <ApolloProvider client={client}>
                        <StaticRouter location={req.url}>
                            <Switch>{routing}</Switch>
                        </StaticRouter>
                    </ApolloProvider>
                </Provider>
            );

            const base = req._parsedUrl.pathname.replace(/^\//g, '').split('/');

            const params = {
                base: '/' + (base.length > 0 ? base[0] + 'assets-' + Date.now() + '/' : ''),
                content,
                script: fs.existsSync(path.resolve('dist/scripts/client.js'))
                    ? './scripts/client.js'
                    : null,
                css: fs.existsSync(path.resolve('dist/styles/client.css'))
                    ? './styles/client.css'
                    : null,
                env: jwt.encode(JSON.stringify(config), NODE_SECRET_KEY),
                state: jwt.encode(
                    JSON.stringify(store.getState()),
                    NODE_SECRET_KEY
                ),
            };

            res.render('index', params);
        });
    });
};
