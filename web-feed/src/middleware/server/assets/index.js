export default app => {
    console.log('assets...');

    app.use((req, res, next) => {
        const temp = req.url.split(/\/assets-\d+\//);
        if (temp.length > 1) {
            req.url = `/${temp[1]}`;
        }

        next();
    });
};
