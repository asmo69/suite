import handlebars from 'express-handlebars';

export default app => {
    console.log('handlebars...');

    app.engine(
        '.hbs',
        handlebars({
            extname: '.hbs',
            defaultLayout: 'index.hbs',
            layoutsDir: 'views'
        })
    );
    app.set('view engine', '.hbs');
    app.set('views', 'views');
};
