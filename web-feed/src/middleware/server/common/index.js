import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';

export default app => {
    console.log('common...');

    app.use(bodyParser.urlencoded({extended: false}));
    app.use(bodyParser.json());
    app.use(cookieParser());
};
