import compression from 'compression';

export default app => {
    console.log('compression...');

    app.use(
        compression({
            level: 9
        })
    );
};
