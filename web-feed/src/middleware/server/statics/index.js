import express from 'express';
import expressStaticGzip from 'express-static-gzip';

export default app => {
    console.log('static...');

    app.use(
        '/',
        expressStaticGzip('dist', {
            index: false,
            enableBrotli: true,
            orderPreference: ['br', 'gz'],
            setHeaders(res) {
                res.setHeader('Cache-Control', 'public, max-age=31536000');
            }
        })
    );
    app.use(express.static('public', {maxAge: '30 days'}));
};
