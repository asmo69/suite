import ip from 'ip';
import fs from 'fs';

import http from 'http';
import https from 'https';

export default (app, env) => {
    console.log('express...');

    const credentials = {
        key: fs.readFileSync('./cert/out.pem', 'utf8'),
        cert: fs.readFileSync('./cert/cert.pem', 'utf8')
    };

    const secure = https.createServer(credentials, app);
    const standard = http.createServer(app);

    secure.listen(env.NODE_HTTPS_PORT || 3000, err => {
        if (err) throw err;
        console.info(
            `🚀 HTTPS server is running: (https://${ip.address()}:${env.NODE_HTTPS_PORT ||
                3000})`
        );
    });
    standard.listen(env.NODE_HTTP_PORT || 4000, err => {
        if (err) throw err;
        console.info(
            `🚀 HTTP server is running: (http://${ip.address()}:${env.NODE_HTTP_PORT ||
                4000})`
        );
    });
};
