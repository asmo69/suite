import express from 'express';

import routes from './src/routes/server';
import env from './src/env/server';

import middlewares from './src/middleware/server';

const app = express();

middlewares.map(middleware => middleware(app, env, routes));
