const webpack = require('webpack');
const dotenv = require('dotenv');

const StyleLintPlugin = require('stylelint-bare-webpack-plugin');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin-fixed-hashbug');
const BrotliPlugin = require('brotli-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const env = dotenv.config().parsed;
const config = {
    ...env,
    ...process.env
};

const COMMIT_HASH = require('child_process')
    .execSync('git rev-parse --short=7 HEAD')
    .toString()
    .trim();

const VERSION = require('./package.json').version;

module.exports = (env, argv) => {
    return [
        {
            name: 'client',
            target: 'web',
            entry: {
                client: __dirname + '/client.js'
            },
            output: {
                path: __dirname + '/dist/scripts',
                filename: '[name].js'
            },
            stats: {
                all: false,
                assets: true,
                errors: true
            },
            resolve: {
                extensions: ['.js', '.jsx']
            },
            plugins: [
                new HardSourceWebpackPlugin(),
                new webpack.DefinePlugin({
                    NODE_SECRET_KEY: JSON.stringify(config.NODE_SECRET_KEY),
                    NODE_ENV: JSON.stringify(argv.mode),
                    COMMIT_HASH: JSON.stringify(COMMIT_HASH),
                    VERSION: JSON.stringify(VERSION)
                }),
                new MiniCssExtractPlugin({
                    filename: '../styles/[name].css'
                }),
                new CompressionPlugin({
                    algorithm: 'gzip',
                    test: /\.js$|\.css$|\.html$/,
                    threshold: 1024,
                    minRatio: 0.7
                }),
                new BrotliPlugin({
                    test: /\.(js|css|html|svg)$/,
                    threshold: 1024,
                    minRatio: 0.8
                }),
                new StyleLintPlugin({
                    files: __dirname + '/src/**/*.scss',
                    emitErrors: true,
                    failOnError: false,
                    quiet: false,
                    syntax: 'scss'
                })
            ],
            module: {
                rules: [
                    {
                        test: /\.jsx?$/,
                        exclude: /node_modules/,
                        loader: 'eslint-loader',
                        enforce: 'pre',
                        options: {
                            configFile: './.eslintrc',
                            fix: true,
                            failOnError: true,
                            failOnWarning: false
                        }

                    },
                    {
                        test: /\.jsx?$/,
                        loader: 'babel-loader',
                        exclude: /node_modules/,
                        query: {
                            presets: [
                                '@babel/preset-react',
                                '@babel/preset-env',
                                'airbnb'
                            ],
                            plugins: [
                                '@babel/plugin-proposal-class-properties',
                                '@babel/plugin-proposal-function-bind',
                                '@babel/plugin-proposal-export-namespace-from',
                                '@babel/plugin-proposal-export-default-from'
                            ]
                        }
                    },
                    {
                        test: /\.txt$/,
                        exclude: /node_modules/,
                        loader: 'raw-loader'
                    },
                    {
                        test: /\.html$/,
                        exclude: /node_modules/,
                        loader: 'html-loader'
                    },
                    {
                        test: /\.(jpe?g|png|gif|svg)$/i,
                        exclude: /node_modules/,
                        loaders: [
                            'file-loader?hash=sha512&digest=hex&name=[hash].[ext]&outputPath=../images/&publicPath=./images/'
                        ]
                    },
                    {
                        test: /\.(eot|ttf|woff|woff2)(\?v=\d+\.\d+\.\d+)?$/i,
                        exclude: /node_modules/,
                        loaders: [
                            'file-loader?hash=sha512&digest=hex&name=[hash].[ext]&outputPath=../fonts/&publicPath=./fonts/'
                        ]
                    },
                    {
                        test: /\.css$/,
                        exclude: /node_modules/,
                        use: [
                            {
                                loader: MiniCssExtractPlugin.loader,
                                options: {
                                    publicPath: '../'
                                }
                            },
                            'css-loader'
                        ]
                    },
                    {
                        test: /\.scss$/,
                        exclude: /node_modules/,
                        use: [
                            {
                                loader: MiniCssExtractPlugin.loader,
                                options: {
                                    publicPath: '../'
                                }
                            },
                            {
                                loader: 'css-loader',
                                options: {
                                    importLoaders: 1,
                                    modules: {
                                        localIdentName: '[name]__[local]--[hash:base64:5]',
                                    }
                                }
                            },
                            {
                                loader: 'sass-loader',
                                options: {
                                    implementation: require('sass')
                                }
                            }
                        ]
                    },
                    {
                        test: /\.less$/,
                        exclude: /node_modules/,
                        use: [
                            {
                                loader: MiniCssExtractPlugin.loader,
                                options: {
                                    publicPath: '../'
                                }
                            },
                            'css-loader?modules&importLoaders',
                            'less-loader'
                        ]
                    }
                ]
            }
        },
        {
            name: 'server',
            target: 'node',
            entry: {
                server: __dirname + '/server.js'
            },
            output: {
                path: __dirname + '/dist',
                filename: '[name].js',
                libraryTarget: 'commonjs2'
            },
            stats: {
                all: false,
                assets: true,
                errors: true
            },
            resolve: {
                extensions: ['.js', '.jsx']
            },
            plugins: [
                new HardSourceWebpackPlugin(),
                new webpack.DefinePlugin({
                    NODE_SECRET_KEY: JSON.stringify(config.NODE_SECRET_KEY),
                    NODE_ENV: JSON.stringify(argv.mode),
                    COMMIT_HASH: JSON.stringify(COMMIT_HASH),
                    VERSION: JSON.stringify(VERSION)
                }),
                new MiniCssExtractPlugin({
                    filename: './styles/[name].css'
                })
            ],
            module: {
                rules: [
                    {
                        test: /\.jsx?$/,
                        exclude: /node_modules/,
                        loader: 'eslint-loader',
                        enforce: 'pre',
                        options: {
                            configFile: './.eslintrc',
                            fix: true,
                            failOnError: true,
                            failOnWarning: false,
                            emitWarnings: false
                        }

                    },
                    {
                        test: /\.jsx?$/,
                        loader: 'babel-loader',
                        exclude: /node_modules/,
                        query: {
                            presets: [
                                '@babel/preset-react',
                                '@babel/preset-env',
                                'airbnb'
                            ],
                            plugins: [
                                '@babel/plugin-proposal-class-properties',
                                '@babel/plugin-proposal-function-bind',
                                '@babel/plugin-proposal-export-namespace-from',
                                '@babel/plugin-proposal-export-default-from'
                            ]
                        }
                    },
                    {
                        test: /\.txt$/,
                        exclude: /node_modules/,
                        loader: 'raw-loader'
                    },
                    {
                        test: /\.html$/,
                        exclude: /node_modules/,
                        loader: 'html-loader'
                    },
                    {
                        test: /\.(jpe?g|png|gif|svg)$/i,
                        exclude: /node_modules/,
                        loaders: [
                            'file-loader?hash=sha512&digest=hex&name=[hash].[ext]&outputPath=./images/&publicPath=./images/'
                        ]
                    },
                    {
                        test: /\.(eot|ttf|woff|woff2)(\?v=\d+\.\d+\.\d+)?$/i,
                        exclude: /node_modules/,
                        loaders: [
                            'file-loader?hash=sha512&digest=hex&name=[hash].[ext]&outputPath=./fonts/&publicPath=./fonts/'
                        ]
                    },
                    {
                        test: /\.css$/,
                        exclude: /node_modules/,
                        use: [
                            {
                                loader: MiniCssExtractPlugin.loader,
                                options: {
                                    publicPath: '../'
                                }
                            },
                            'css-loader'
                        ]
                    },
                    {
                        test: /\.scss$/,
                        exclude: /node_modules/,
                        use: [
                            {
                                loader: MiniCssExtractPlugin.loader,
                                options: {
                                    publicPath: '../'
                                }
                            },
                            {
                                loader: 'css-loader',
                                options: {
                                    importLoaders: 1,
                                    modules: {
                                        localIdentName: '[name]__[local]--[hash:base64:5]',
                                    }
                                }
                            },
                            {
                                loader: 'sass-loader',
                                options: {
                                    implementation: require('sass')
                                }
                            }
                        ]
                    },
                    {
                        test: /\.less$/,
                        exclude: /node_modules/,
                        use: [
                            {
                                loader: MiniCssExtractPlugin.loader,
                                options: {
                                    publicPath: '../'
                                }
                            },
                            'css-loader?modules&importLoaders',
                            'less-loader'
                        ]
                    }
                ]
            }
        }
    ];
}