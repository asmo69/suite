import routes from './src/routes/client';
import env from './src/env/client';

import middlewares from './src/middleware/client';

middlewares.map(middleware => middleware(env, routes));
