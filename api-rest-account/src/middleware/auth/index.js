const jwt = require("jwt-simple");
const dotenv = require("dotenv");

const env = dotenv.config().parsed;
const config = {
  ...env,
  ...process.env,
};

module.exports = (req, res, next) => {
  const token = (
    req.headers.authorization ||
    req.cookies.authorization ||
    ""
  ).replace(/^Bearer\s+/i, "");
  if (token) {
    try {
      const payload = jwt.decode(token, config.NODE_AUTH0_SECRET_KEY);
      req.authorization = {
        user: {
          email: payload["http://workbench/uid"],
          name: payload["http://workbench/displayname"],
          organisation: payload["http://workbench/organisation"],
          groups: payload["http://workbench/groups"],
        },
      };
    } catch (error) {
      req.authorization = null;

      console.error(error);
    }
  } else {
    req.authorization = null;
  }

  console.log(req.authorization);

  next();
};
