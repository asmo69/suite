const dotenv = require("dotenv");
const { Sequelize } = require("sequelize");

const env = dotenv.config().parsed;
const config = {
  ...env,
  ...process.env,
};

module.exports = (req, res, next) => {
  const dialect = config.DATABASE_TYPE || "mysql";
  const sequelize = new Sequelize(
    config.DATABASE_NAME || "mysql",
    config.DATABASE_USER || "mysql",
    config.DATABASE_PASSWORD || "mysql",
    {
      host: config.DATABASE_HOST || "127.0.0.1",
      dialect: config.DATABASE_TYPE || "mysql",
      dialectModule: dialect === "mysql" ? require("mysql2") : null,
      define: {
        timestamps: false,
      },
      logging: console.log,
    }
  );

  req.sequelize = sequelize;

  next();
};
