const home = require("../actions/home");
const whoami = require("../actions/whoami");

module.exports = [
  {
    path: "/whoami",
    action: whoami,
  },
  {
    path: "/",
    action: home,
  },
];
