module.exports = (req, res) => {
  if (req.authorization && req.authorization.user) {
    res.send({
      status: "ok",
      count: 1,
      rows: [req.authorization.user],
    });
  } else {
    res.send({
      status: "ok",
      count: 0,
      rows: [],
    });
  }
};
