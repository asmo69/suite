const QueryBuilder = function (type = 'SELECT') {
    this.data = {
        type,
        attributes: [],
        from: 'table',
        joins: [],
        where: [],
        having: [],
        replacements: [],
        order: [],
        group: [],
        offset: null,
        limit: null,
    };
};

QueryBuilder.Op = {
    Equals: '=',
    NotEquals: '!=',
    GreaterThan: '>',
    LessThan: '<',
    GreaterOrEqual: '>=',
    LessOrEqual: '<=',
    In: 'IN',
    NotIn: 'NOT IN',
    Like: 'LIKE',
    NotLike: 'NOT LIKE',
    All: 'ALL',
    None: 'ALL',
};

QueryBuilder.select = function () {
    return new QueryBuilder('SELECT');
};

QueryBuilder.prototype.attributes = function (name) {
    this.data.attributes.push(name);
    return this;
};

QueryBuilder.prototype.setAttributes = function (name) {
    this.data.attributes = [name];
    return this;
};

QueryBuilder.prototype.from = function (name) {
    this.data.from = name;
    return this;
};

QueryBuilder.prototype.join = function (name, clause, type = '') {
    this.data.joins.push({name, clause, type});
    return this;
};

QueryBuilder.prototype.where = function (name, value, op = '=') {
    if (value !== undefined) {
        if (op === QueryBuilder.Op.All) {
            if (Array.isArray(value)) {
                for (let x = 0; x < value.length; x++) {
                    this.data.where.push({
                        name,
                        value: value[x],
                        op: QueryBuilder.Op.Equals,
                    });
                    this.data.replacements.push(value[x]);
                }
            } else {
                this.data.where.push({name, value, op: QueryBuilder.Op.Equals});
                this.data.replacements.push(value);
            }
        } else if (op === QueryBuilder.Op.None) {
            if (Array.isArray(value)) {
                for (let x = 0; x < value.length; x++) {
                    this.data.where.push({
                        name,
                        value: value[x],
                        op: QueryBuilder.Op.NotEquals,
                    });
                    this.data.replacements.push(value[x]);
                }
            } else {
                this.data.where.push({name, value, op: QueryBuilder.Op.Equals});
                this.data.replacements.push(value);
            }
        } else if (Array.isArray(value)) {
            if (op === QueryBuilder.Op.NotEquals) {
                this.data.where.push({name, value, op: QueryBuilder.Op.NotIn});
            } else {
                this.data.where.push({name, value, op: QueryBuilder.Op.In});
            }
            this.data.replacements.push(value);
        } else if (
            value.toString().startsWith('%') ||
            value.toString().endsWith('%')
        ) {
            if (op === QueryBuilder.Op.NotEquals) {
                this.data.where.push({
                    name,
                    value,
                    op: QueryBuilder.Op.NotLike,
                });
            } else {
                this.data.where.push({name, value, op: QueryBuilder.Op.Like});
            }
            this.data.replacements.push(value);
        } else {
            this.data.where.push({name, value, op});
            this.data.replacements.push(value);
        }
    }
    return this;
};

QueryBuilder.prototype.having = function (name, value, op = '=') {
    if (value !== undefined) {
        if (op === QueryBuilder.Op.All) {
            if (Array.isArray(value)) {
                for (let x = 0; x < value.length; x++) {
                    this.data.having.push({
                        name,
                        value: value[x],
                        op: QueryBuilder.Op.Equals,
                    });
                    this.data.replacements.push(value[x]);
                }
            } else {
                this.data.having.push({
                    name,
                    value,
                    op: QueryBuilder.Op.Equals,
                });
                this.data.replacements.push(value);
            }
        } else if (op === QueryBuilder.Op.None) {
            if (Array.isArray(value)) {
                for (let x = 0; x < value.length; x++) {
                    this.data.having.push({
                        name,
                        value: value[x],
                        op: QueryBuilder.Op.NotEquals,
                    });
                    this.data.replacements.push(value[x]);
                }
            } else {
                this.data.having.push({
                    name,
                    value,
                    op: QueryBuilder.Op.Equals,
                });
                this.data.replacements.push(value);
            }
        } else if (Array.isArray(value)) {
            if (op === QueryBuilder.Op.NotEquals) {
                this.data.having.push({name, value, op: QueryBuilder.Op.NotIn});
            } else {
                this.data.having.push({name, value, op: QueryBuilder.Op.In});
            }
            this.data.replacements.push(value);
        } else if (
            value.toString().startsWith('%') ||
            value.toString().endsWith('%')
        ) {
            if (op === QueryBuilder.Op.NotEquals) {
                this.data.having.push({
                    name,
                    value,
                    op: QueryBuilder.Op.NotLike,
                });
            } else {
                this.data.having.push({name, value, op: QueryBuilder.Op.Like});
            }
            this.data.replacements.push(value);
        } else {
            this.data.having.push({name, value, op});
            this.data.replacements.push(value);
        }
    }
    return this;
};

QueryBuilder.prototype.order = function (name, direction = '') {
    if (name !== undefined) {
        this.data.order.push({name, direction});
    }
    return this;
};

QueryBuilder.prototype.group = function (name, direction = '') {
    if (name !== undefined) {
        this.data.group.push({name, direction});
    }
    return this;
};

QueryBuilder.prototype.offset = function (value) {
    if (typeof value !== undefined && value !== null) {
        this.data.offset = value;
    }
    return this;
};

QueryBuilder.prototype.limit = function (value) {
    if (typeof value !== undefined && value !== null) {
        this.data.limit = value;
    }
    return this;
};

QueryBuilder.prototype.build = function () {
    const sql = [
        this.data.type,
        this.data.attributes.length ? this.data.attributes.join(', ') : '*',
        'FROM',
        this.data.from,
    ];
    if (this.data.joins.length) {
        for (let x = 0; x < this.data.joins.length; x++) {
            sql.push(
                this.data.joins[x].type +
                    ' JOIN ' +
                    this.data.joins[x].name +
                    ' ON (' +
                    this.data.joins[x].clause +
                    ')'
            );
        }
    }
    if (this.data.where.length) {
        const where = [];
        for (let x = 0; x < this.data.where.length; x++) {
            if (
                this.data.where[x].op === QueryBuilder.Op.In ||
                this.data.where[x].op === QueryBuilder.Op.NotIn
            ) {
                where.push(
                    this.data.where[x].name +
                        ' ' +
                        this.data.where[x].op +
                        ' (?)'
                );
            } else {
                where.push(
                    this.data.where[x].name + ' ' + this.data.where[x].op + ' ?'
                );
            }
        }
        sql.push('WHERE');
        sql.push(where.join(' AND '));
    }
    if (this.data.group.length) {
        const group = [];
        for (let x = 0; x < this.data.group.length; x++) {
            group.push(
                this.data.group[x].name + ' ' + this.data.group[x].direction
            );
        }
        sql.push('GROUP BY');
        sql.push(group.join(', '));
    }
    if (this.data.having.length) {
        const having = [];
        for (let x = 0; x < this.data.having.length; x++) {
            having.push(
                this.data.having[x].name + ' ' + this.data.having[x].op + ' ?'
            );
        }
        sql.push('HAVING');
        sql.push(having.join(' AND '));
    }
    if (this.data.order.length) {
        const order = [];
        for (let x = 0; x < this.data.order.length; x++) {
            order.push(
                this.data.order[x].name + ' ' + this.data.order[x].direction
            );
        }
        sql.push('ORDER BY');
        sql.push(order.join(', '));
    }
    if (this.data.offset || this.data.limit) {
        sql.push('LIMIT');
        sql.push(this.data.offset + ', ' + this.data.limit);
    }

    return sql.join(' ');
};

QueryBuilder.prototype.count = function () {
    const sql = [
        this.data.type,
        'COUNT(',
        this.data.attributes.length ? this.data.attributes.join(', ') : '*',
        ') AS count',
        'FROM',
        this.data.from,
    ];
    if (this.data.joins.length) {
        for (let x = 0; x < this.data.joins.length; x++) {
            sql.push(
                this.data.joins[x].type +
                    ' JOIN ' +
                    this.data.joins[x].name +
                    ' ON (' +
                    this.data.joins[x].clause +
                    ')'
            );
        }
    }
    if (this.data.where.length) {
        const where = [];
        for (let x = 0; x < this.data.where.length; x++) {
            if (
                this.data.where[x].op === QueryBuilder.Op.In ||
                this.data.where[x].op === QueryBuilder.Op.NotIn
            ) {
                where.push(
                    this.data.where[x].name +
                        ' ' +
                        this.data.where[x].op +
                        ' (?)'
                );
            } else {
                where.push(
                    this.data.where[x].name + ' ' + this.data.where[x].op + ' ?'
                );
            }
        }
        sql.push('WHERE');
        sql.push(where.join(' AND '));
    }
    if (this.data.group.length) {
        const group = [];
        for (let x = 0; x < this.data.group.length; x++) {
            group.push(
                this.data.group[x].name + ' ' + this.data.group[x].direction
            );
        }
        sql.push('GROUP BY');
        sql.push(group.join(', '));
    }
    if (this.data.having.length) {
        const having = [];
        for (let x = 0; x < this.data.having.length; x++) {
            having.push(
                this.data.having[x].name + ' ' + this.data.having[x].op + ' ?'
            );
        }
        sql.push('HAVING');
        sql.push(having.join(' AND '));
    }

    return sql.join(' ');
};

QueryBuilder.prototype.replacements = function () {
    return this.data.replacements;
};

module.exports = QueryBuilder;
