const home = require("../actions/home");
const document = require("../actions/document");
const page = require("../actions/page");
const meta = require("../actions/meta");
const entity = require("../actions/entity");

module.exports = [
  {
    path: "/document",
    action: document,
  },
  {
    path: "/page",
    action: page,
  },
  {
    path: "/meta",
    action: meta,
  },
  {
    path: "/entity",
    action: entity,
  },
  {
    path: "/",
    action: home,
  },
];
