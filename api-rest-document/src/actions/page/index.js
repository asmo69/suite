const { QueryTypes } = require("sequelize");
const QueryBuilder = require("../../query-builder");

module.exports = (req, res) => {
  const { sequelize, query } = req;

  const limit = Math.min(
    isNaN(parseInt(query.limit, 10)) ? 20 : parseInt(query.limit, 10),
    20
  );
  const offset = isNaN(parseInt(query.offset, 10))
    ? 0
    : parseInt(query.offset, 10);

  const args = {
    ...req.query,
    ...req.params,
  };

  const q = QueryBuilder.select()
    .from("page p")
    .where("p.id", args.id)
    .where("p.document_id", args.document_id)
    .where("p.language", args.language)
    .where("p.page", args.page)
    .order(args.order, args.direction)
    .offset(offset)
    .limit(limit);

  Promise.all([
    sequelize.query(q.count(), {
      replacements: q.replacements(),
      type: QueryTypes.SELECT,
    }),
    sequelize.query(q.build(), {
      replacements: q.replacements(),
      type: QueryTypes.SELECT,
    }),
  ]).then(([count, rows]) =>
    res.send({
      status: "ok",
      limit,
      offset,
      count: count[0].count,
      rows,
    })
  );
};
