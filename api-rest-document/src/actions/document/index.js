const { QueryTypes } = require("sequelize");
const QueryBuilder = require("../../query-builder");

module.exports = (req, res) => {
  const { sequelize, query } = req;

  const limit = Math.min(
    isNaN(parseInt(query.limit, 10)) ? 20 : parseInt(query.limit, 10),
    20
  );
  const offset = isNaN(parseInt(query.offset, 10))
    ? 0
    : parseInt(query.offset, 10);

  const args = {
    ...req.query,
    ...req.params,
  };

  const q = QueryBuilder.select()
    .from("document d")
    .where("d.id", args.id)
    .where("d.uuid", args.uuid)
    .where("d.language", args.language)
    .where("d.source", args.source)
    .where("d.subsource", args.subsource)
    .where("d.mime", args.mime)
    .where("d.title", args.title)
    .where("d.published", args.publishedStart, QueryBuilder.Op.GreaterOrEqual)
    .where("d.published", args.publishedEnd, QueryBuilder.Op.LessOrEqual)
    .where("d.created", args.createdStart, QueryBuilder.Op.GreaterOrEqual)
    .where("d.created", args.createdEnd, QueryBuilder.Op.LessOrEqual)
    .where("d.updated", args.updatedStart, QueryBuilder.Op.GreaterOrEqual)
    .where("d.updated", args.updatedEnd, QueryBuilder.Op.LessOrEqual)
    .order(args.order, args.direction)
    .offset(offset)
    .limit(limit);

  if (args.entity_id) {
    q.join("document_entity de", "d.id = de.document_id");
    q.where("de.entity_id", args.entity_id);
  }
  if (args.all_entity_id) {
    q.join("document_entity de", "d.id = de.document_id");
    q.where("de.entity_id", args.all_entity_id, QueryBuilder.Op.All);
  }

  console.log(q.setAttributes("DISTINCT d.*").build());
  console.log(q.replacements());

  Promise.all([
    sequelize.query(q.setAttributes("DISTINCT d.id").count(), {
      replacements: q.replacements(),
      type: QueryTypes.SELECT,
    }),
    sequelize.query(q.setAttributes("DISTINCT d.*").build(), {
      replacements: q.replacements(),
      type: QueryTypes.SELECT,
    }),
  ]).then(([count, rows]) =>
    res.send({
      status: "ok",
      limit,
      offset,
      count: count[0].count,
      rows,
    })
  );
};
