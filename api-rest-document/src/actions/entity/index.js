const { QueryTypes } = require("sequelize");
const QueryBuilder = require("../../query-builder");

module.exports = (req, res) => {
  const { sequelize, query } = req;

  const limit = Math.min(
    isNaN(parseInt(query.limit, 10)) ? 20 : parseInt(query.limit, 10),
    20
  );
  const offset = isNaN(parseInt(query.offset, 10))
    ? 0
    : parseInt(query.offset, 10);

  const args = {
    ...req.query,
    ...req.params,
  };

  const q = QueryBuilder.select()
    .from("entity e")
    .where("e.id", args.id)
    .where("e.name", args.name)
    .where("e.type", args.type)
    .order(args.order, args.direction)
    .offset(offset)
    .limit(limit);

  if (args.document_id) {
    q.join("document_entity de", "e.id = de.entity_id");
    q.where("de.document_id", args.document_id);
  }

  Promise.all([
    sequelize.query(q.setAttributes("DISTINCT e.id").count(), {
      replacements: q.replacements(),
      type: QueryTypes.SELECT,
    }),
    sequelize.query(q.setAttributes("DISTINCT e.*").build(), {
      replacements: q.replacements(),
      type: QueryTypes.SELECT,
    }),
  ]).then(([count, rows]) =>
    res.send({
      status: "ok",
      limit,
      offset,
      count: count[0].count,
      rows,
    })
  );
};
