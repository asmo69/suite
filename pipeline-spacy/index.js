const pkg = require('./package.json');
const env = require('./src/env');
const Kafka = require('./src/kafka');
const spacy = require('./src/spacy');

const topics = {
    produce: env.KAFKA_PRODUCE || null,
    consume: env.KAFKA_CONSUME || null
};

const app = new Kafka();

const main = async () => {
    app.connect({
        clientId: pkg.name,
        brokers: [env.KAFKA_HOST || '127.0.0.1:9092']
    });

    app.use(topics.consume, async (topic, message) => {
        if (message.page && message.page.text && message.language === 'en') {
            message.entities = await spacy(
                env.SPACY_HOST || '127.0.0.1:8080',
                message.page.text
            );
        } else {
            message.entities = [];
        }
        await app.send(topics.produce, message);
    });

    app.start();
};

main().catch((error) => {
    console.error(`💣 Error... %s`, error.message || error);
    app.disconnect();
    process.exit(0);
});
