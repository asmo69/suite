const fetch = require('isomorphic-fetch');
const fetchRetry = require('fetch-retry')(fetch);

module.exports = (host, text, model = 'en') => {
    const url = 'http://' + host + '/ent';

    return fetchRetry(url, {
        retries: 5,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            text,
            model
        })
    })
        .then((response) => response.json())
        .catch(() => []);
};
