const {Kafka, logLevel, CompressionTypes} = require('kafkajs');

module.exports = class {
    constructor() {
        this.options = {};
        this.client = null;
        this.producer = null;
        this.consumer = null;
        this.data = [];
    }

    connect(options = {}) {
        console.info(`🚀 Running...`);

        this.options = options;
        this.client = new Kafka({
            clientId: options.clientId,
            brokers: options.brokers,
            logLevel: logLevel.NOTHING
        });
    }

    disconnect() {
        console.info(`🏁 Finished...`);

        if (this.consumer) {
            this.consumer.disconnect();
        }
        if (this.producer) {
            this.producer.disconnect();
        }
    }

    use(...args) {
        if (args.length == 1 && typeof args[0] === 'function') {
            this.data.push({topic: '*', callback: args[0]});
        } else if (
            args.length == 2 &&
            typeof args[0] === 'string' &&
            typeof args[1] === 'function'
        ) {
            this.data.push({topic: args[0], callback: args[1]});
        }
    }

    parse(message) {
        try {
            return JSON.parse(message);
        } catch (error) {
            // Do Nothing
        }
        return {
            value: message
        };
    }

    async process(topic, message) {
        console.info(`🔧 Processing from ${topic}...`);

        for (let x = 0; x < this.data.length; x++) {
            if (this.data[x].topic === '*' || this.data[x].topic === topic) {
                await this.data[x].callback(topic, message);
            }
        }
    }

    async send(topic, message) {
        console.info(`📤 Sending to ${topic}...`);

        if (!this.producer) {
            this.producer = this.client.producer();
            await this.producer.connect();
        }
        await this.producer.send({
            topic: topic,
            compression: CompressionTypes.GZIP,
            messages: [
                {
                    value: JSON.stringify(message)
                }
            ]
        });
    }

    async start() {
        const topics = this.data
            .map((item) => item.topic)
            .filter((value, index, self) => self.indexOf(value) === index);
        const regex = new RegExp('^(' + topics.join('|') + ')$');

        if (!this.consumer) {
            this.consumer = this.client.consumer({
                groupId: this.options.groupId || this.options.clientId
            });
            await this.consumer.connect();
        }

        await this.consumer.subscribe({
            topic: regex
        });

        await this.consumer.run({
            eachMessage: ({topic, message}) => {
                console.info(`📥 Recieving from ${topic}...`);
                return this.process(topic, this.parse(message.value));
            }
        });
    }
};
