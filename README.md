# The Pipieline

![](./diagram1.png)
![](./diagram2.png)

## Install

git clone https://asmo69@bitbucket.org/asmo69/adarga-suite.git

## Run

docker-compose up

## Software

### Mysql
### Redis
### Elastic Search

### Spacy

Spacy is an NLP service which extracts and returns entities (names, date, places, events, etc) from a block of text.

### Kafka 

Kafka is a message queuingand logging service. Your program can create a number messages' which it can then post to a number of 'topics'. Another program can listen to one or more of these 'topics' and receive any 'messages' that are posted there before processing and passing them on to another 'topic'.

Think of it like a industrial wharehouse assembly line.... with each person assembling one part of a product and passing it on to the next via a conveyor belt.

## Approach

Using Kafka to provide message queues allows me to take a 'micro-service' approach to creating a document processing pipeline.

Each service within the pipeline can focus on performing on small job before passing the message on to the next.

## Services

### Ingestion Services

There are currently four services which initiated the injestion pipeline.

Each service represented a injesgting data from a different source or feed.

Additional and bespoke pipeline services can be added here to ingest documents from any source a client wishes.

#### pipeline-fake

This services runs every 5 minutes and injects a 'fake' news story into Kafka in order to test the pipeline.

#### pipeline-import

This services can be used to import a large text document and break it into small 'pages' that can be injected into Kafka. This is something that Adarga currently struggles with.

#### pipeline-newsapi

This service runs every 5 minutes and loads the data feed from http://www.newsapi.org and injects the news stories in Kafka.

#### pipeline-wehbose

This service runs every 5 minutes and loads the data feed from http://www.webhose.io and injects the news stories in Kafka.

### Processing Services

These services read messages from one topic, process the message and then pass it on.

#### pipeline-spacy

This services processes messages using service-spacy. This service uses NLP and AI to extracts entities (names, date, places, events, etc) from a block of text. These entities are then attached the mesage before it is passed along the pipeline.

#### pipeline-markup

This services process messages and creates HTML style markup using the entities supplied by the previous service. This markup is added to the message before it is passed along the piupeline.

### Storage Services

These services are the end of the pipeline. Messages recieved here have now been fully processed and can be stored.

#### pipeline-mysql

This services process messages and stores them in a normalized mysql database.

#### pipeline-file

This services process messages and stores them in JSON encoded text files.

