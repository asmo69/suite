/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "document_entity",
    {
      document_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        comment: "null",
        references: {
          model: "document",
          key: "id",
        },
      },
      entity_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        comment: "null",
        references: {
          model: "entity",
          key: "id",
        },
      },
    },
    {
      tableName: "document_entity",
    }
  );
};
