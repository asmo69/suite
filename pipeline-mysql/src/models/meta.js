/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "meta",
    {
      id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        comment: "null",
        autoIncrement: true,
      },
      document_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        comment: "null",
        references: {
          model: "document",
          key: "id",
        },
      },
      name: {
        type: DataTypes.STRING(50),
        allowNull: false,
        defaultValue: "",
        comment: "null",
      },
      value: {
        type: DataTypes.STRING(255),
        allowNull: false,
        defaultValue: "",
        comment: "null",
      },
    },
    {
      tableName: "meta",
    }
  );
};
