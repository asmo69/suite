/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "document",
    {
      id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        comment: "null",
        autoIncrement: true,
      },
      uuid: {
        type: DataTypes.STRING(100),
        allowNull: false,
        comment: "null",
      },
      language: {
        type: DataTypes.STRING(100),
        allowNull: false,
        defaultValue: "en",
        comment: "null",
      },
      source: {
        type: DataTypes.STRING(100),
        allowNull: false,
        defaultValue: "",
        comment: "null",
      },
      subsource: {
        type: DataTypes.STRING(100),
        allowNull: false,
        defaultValue: "",
        comment: "null",
      },
      mime: {
        type: DataTypes.STRING(100),
        allowNull: false,
        defaultValue: "",
        comment: "null",
      },
      published: {
        type: DataTypes.DATE,
        allowNull: false,
        comment: "null",
      },
      created: {
        type: DataTypes.DATE,
        allowNull: false,
        comment: "null",
      },
      updated: {
        type: DataTypes.DATE,
        allowNull: false,
        comment: "null",
      },
      pages: {
        type: DataTypes.BIGINT,
        allowNull: false,
        defaultValue: "1",
        comment: "null",
      },
      url: {
        type: DataTypes.STRING(512),
        allowNull: false,
        defaultValue: "",
        comment: "null",
      },
      title: {
        type: DataTypes.STRING(512),
        allowNull: false,
        defaultValue: "",
        comment: "null",
      },
      image: {
        type: DataTypes.STRING(512),
        allowNull: false,
        defaultValue: "",
        comment: "null",
      },
    },
    {
      tableName: "document",
    }
  );
};
