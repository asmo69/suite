/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "page",
    {
      id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        comment: "null",
        autoIncrement: true,
      },
      document_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        comment: "null",
        references: {
          model: "document",
          key: "id",
        },
      },
      page: {
        type: DataTypes.BIGINT,
        allowNull: false,
        defaultValue: "1",
        comment: "null",
      },
      language: {
        type: DataTypes.STRING(100),
        allowNull: false,
        defaultValue: "en",
        comment: "null",
      },
      text: {
        type: DataTypes.TEXT,
        allowNull: false,
        comment: "null",
      },
      markup: {
        type: DataTypes.TEXT,
        allowNull: false,
        comment: "null",
      },
    },
    {
      tableName: "page",
    }
  );
};
