/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "entity",
    {
      id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        comment: "null",
        autoIncrement: true,
      },
      name: {
        type: DataTypes.STRING(255),
        allowNull: false,
        comment: "null",
        unique: true,
      },
      type: {
        type: DataTypes.STRING(50),
        allowNull: false,
        comment: "null",
      },
      text: {
        type: DataTypes.STRING(255),
        allowNull: false,
        comment: "null",
      },
    },
    {
      tableName: "entity",
    }
  );
};
