const pkg = require('./package.json');

const dotenv = require('dotenv');

const {Kafka} = require('kafkajs');
const {Sequelize, DataTypes} = require('sequelize');

const documentModel = require('./src/models/document');
const pageModel = require('./src/models/page');
const documentEntityModel = require('./src/models/document_entity');
const metaModel = require('./src/models/meta');
const entityModel = require('./src/models/entity');

const env = dotenv.config().parsed;
const topics = {
    produce: process.env.KAFKA_PRODUCE || env.KAFKA_PRODUCE || null,
    consume: process.env.KAFKA_CONSUME || env.KAFKA_CONSUME || null,
};

const dialect = process.env.DATABASE_TYPE || env.DATABASE_TYPE || 'mysql';

const sequelize = new Sequelize(
    process.env.DATABASE_NAME || env.DATABASE_NAME || 'mysql',
    process.env.DATABASE_USER || env.DATABASE_USER || 'mysql',
    process.env.DATABASE_PASSWORD || env.DATABASE_PASSWORD || 'mysql',
    {
        host: process.env.DATABASE_HOST || env.DATABASE_HOST || '127.0.0.1',
        dialect: process.env.DATABASE_TYPE || env.DATABASE_TYPE || 'mysql',
        dialectModule: dialect === 'mysql' ? require('mysql2') : null,
        define: {
            timestamps: false,
        },
        logging: false,
    }
);

const kafka = new Kafka({
    clientId: pkg.name,
    brokers: [process.env.KAFKA_HOST || env.KAFKA_HOST || '127.0.0.1:9092'],
});

const consumer = kafka.consumer({
    groupId: pkg.name + '-' + Date.now(),
});

const pipeline = async (value) => {
    try {
        console.info(
            `🔧 Processing from ${topics.consume}... ${value.uuid}/${value.language}/${value.page.page}`
        );

        console.info(
            `📝 Writing... ${value.uuid}/${value.language}/${value.page.page}`
        );

        const [document] = await sequelize.models.document.findOrCreate({
            where: {
                uuid: value.uuid,
                language: value.language,
            },
            defaults: {
                source: value.source,
                subsource: value.subsource,
                mime: value.mime,
                language: value.language,
                published: value.published,
                created: value.created,
                updated: value.updated,
                pages: value.pages,

                url: value.url,
                title: value.title,
                image: value.image,
            },
        });

        if (value.page) {
            await sequelize.models.page.findOrCreate({
                where: {
                    document_id: document.id,
                    language: value.language,
                    page: value.page.page,
                },
                defaults: {
                    document_id: document.id,
                    language: value.language,
                    page: value.page.page,
                    text: value.page.text || '',
                    markup: value.page.markup || '',
                },
            });
        }

        if (value.entities) {
            for (let x = 0; x < value.entities.length; x++) {
                const [entity] = await sequelize.models.entity.findOrCreate({
                    where: {
                        name:
                            value.entities[x].type +
                            '/' +
                            value.entities[x].text,
                    },
                    defaults: {
                        name:
                            value.entities[x].type +
                            '/' +
                            value.entities[x].text,
                        type: value.entities[x].type,
                        text: value.entities[x].text,
                    },
                });

                await sequelize.models.document_entity.findOrCreate({
                    where: {
                        document_id: document.id,
                        entity_id: entity.id,
                    },
                    defaults: {
                        document_id: document.id,
                        entity_id: entity.id,
                    },
                });
            }
        }

        if (value.meta) {
            const keys = Object.keys(value.meta);
            for (let x = 0; x < keys.length; x++) {
                await sequelize.models.meta.findOrCreate({
                    where: {
                        document_id: document.id,
                        name: keys[x],
                    },
                    defaults: {
                        document_id: document.id,
                        name: keys[x],
                        value: value.meta[keys[x]],
                    },
                });
            }
        }

        console.info(`✅ Success...`);
    } catch (error) {
        console.error(`💣 Error... `);
        console.error(error);
    }
};

const main = async () => {
    console.info(`🚀 Running...`);

    documentModel(sequelize, DataTypes);
    pageModel(sequelize, DataTypes);
    documentEntityModel(sequelize, DataTypes);
    metaModel(sequelize, DataTypes);
    entityModel(sequelize, DataTypes);

    await consumer.connect();
    await consumer.subscribe({
        topic: topics.consume,
        fromBeginning: true,
    });
    await consumer.run({
        eachMessage: async ({message}) => {
            const value = JSON.parse(message.value.toString());

            await pipeline(value);
        },
    });
};

main().catch((error) => {
    console.error(`💣 Error... `);
    console.error(error);
    consumer.disconnect();
    process.exit(0);
});
