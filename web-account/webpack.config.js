const webpack = require('webpack');

const HardSourceWebpackPlugin = require('hard-source-webpack-plugin-fixed-hashbug');

const COMMIT_HASH = require('child_process')
    .execSync('git rev-parse --short=7 HEAD')
    .toString()
    .trim();

const VERSION = require('./package.json').version;

module.exports = (env, argv) => {
    return [
        {
            name: 'server',
            target: 'node',
            entry: {
                index: __dirname + '/index.js'
            },
            output: {
                path: __dirname + '/dist',
                filename: '[name].js',
                libraryTarget: 'commonjs2'
            },
            stats: {
                all: false,
                assets: true,
                errors: true
            },
            plugins: [
                new HardSourceWebpackPlugin(),
                new webpack.DefinePlugin({
                    NODE_ENV: JSON.stringify(argv.mode),
                    COMMIT_HASH: JSON.stringify(COMMIT_HASH),
                    VERSION: JSON.stringify(VERSION)
                })
            ],
            module: {
                rules: [
                    {
                        test: /\.jsx?$/,
                        exclude: /node_modules/,
                        loader: 'eslint-loader',
                        enforce: 'pre',
                        options: {
                            configFile: './.eslintrc',
                            fix: true,
                            failOnError: true,
                            failOnWarning: true
                        }

                    },
                    {
                        test: /\.jsx?$/,
                        loader: 'babel-loader',
                        query: {
                            compact: false
                        }
                    },
                ],
            }
        }
    ];
}