const pkg = require('./package.json');
const env = require('./src/env');
const Kafka = require('./src/kafka');
const translate = require('./src/translate');

const topics = {
    produce: env.KAFKA_PRODUCE || null,
    consume: env.KAFKA_CONSUME || null
};

const app = new Kafka();

const main = async () => {
    app.connect({
        clientId: pkg.name,
        brokers: [env.KAFKA_HOST || '127.0.0.1:9092']
    });

    app.use(topics.consume, async (topic, message) => {
        if (message.language !== 'en') {
            const title = await translate(message.title || '', {
                from: message.language,
                to: 'en'
            });
            const text = await translate(message.page.text || '', {
                from: message.language,
                to: 'en'
            });
            if (title.status === 'ok' && text.status === 'ok') {
                const translation = {
                    ...message,
                    language: 'en',
                    title: title.text,

                    page: {
                        ...message.page,
                        text: text.text,
                        language: 'en'
                    }
                };
                await app.send(topics.produce, translation);
            }
        }

        await app.send(topics.produce, message);
    });

    app.start();
};

main().catch((error) => {
    console.error(`💣 Error... %s`, error.message || error);
    app.disconnect();
    process.exit(0);
});
