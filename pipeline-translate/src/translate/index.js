const translateFree = require('google-translate-free');

module.exports = (text, options) =>
    translateFree(text, options)
        .then((response) => {
            return {
                status: 'ok',
                text: response.text
            };
        })
        .catch(() => {
            return {
                status: 'error',
                text: text
            };
        });
