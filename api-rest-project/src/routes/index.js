const home = require("../actions/home");

module.exports = [
  {
    path: "/",
    action: home,
  },
];
