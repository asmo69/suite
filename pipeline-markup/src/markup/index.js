const htmlentities = (str) => {
    return String(str)
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;');
};

module.exports = (text = '', entities = []) => {
    var retval = '';
    var start = 0;
    for (let x = 0; x < entities.length; x++) {
        if (start != entities[x].start) {
            retval += htmlentities(text.substring(start, entities[x].start));
        }
        retval +=
            '<cite data-entity-id="' +
            htmlentities(entities[x].type) +
            '/' +
            htmlentities(entities[x].text) +
            '" data-entity-type="' +
            htmlentities(entities[x].type) +
            '">' +
            htmlentities(text.substring(entities[x].start, entities[x].end)) +
            '</cite>';
        start = entities[x].end;
    }
    retval += htmlentities(text.substr(start));
    return retval;
};
