const ip = require("ip");
const fs = require("fs");
const dotenv = require("dotenv");

const express = require("express");
const http = require("http");
const https = require("https");

const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");

const env = dotenv.config().parsed;
const config = {
  ...env,
  ...process.env,
};

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());

app.get("/", (req, res) => {
  res.send("Home Page");
});

const credentials = {
  key: fs.readFileSync("./cert/out.pem", "utf8"),
  cert: fs.readFileSync("./cert/cert.pem", "utf8"),
};

const secure = https.createServer(credentials, app);
const standard = http.createServer(app);

secure.listen(config.NODE_HTTPS_PORT || 3000, (err) => {
  if (err) throw err;
  console.info(
    `🚀 HTTPS server is running: (https://${ip.address()}:${
      config.NODE_HTTPS_PORT || 3000
    })`
  );
});
standard.listen(config.NODE_HTTP_PORT || 4000, (err) => {
  if (err) throw err;
  console.info(
    `🚀 HTTP server is running: (http://${ip.address()}:${
      config.NODE_HTTP_PORT || 4000
    })`
  );
});
