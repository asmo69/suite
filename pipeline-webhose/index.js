const pkg = require("./package.json");

const md5 = require("md5");
const dotenv = require("dotenv");
const fetch = require("isomorphic-fetch");
const fetchRetry = require("fetch-retry")(fetch);
const ISO6391 = require("iso-639-1");

const { Kafka } = require("kafkajs");

const env = dotenv.config().parsed;
const topics = {
  produce: process.env.KAFKA_PRODUCE || env.KAFKA_PRODUCE || null,
  consume: process.env.KAFKA_CONSUME || env.KAFKA_CONSUME || null,
};

const url =
  "https://webhose.io/filterWebContent?token=39c4334d-c276-40de-9aa4-898c5b85f404&format=jsonsort=crawled&q=site_type%3Anews&latest=true";

const kafka = new Kafka({
  clientId: pkg.name,
  brokers: [process.env.KAFKA_HOST || env.KAFKA_HOST || "127.0.0.1:9092"],
});

const producer = kafka.producer();

const main = async () => {
  console.info(`🚀 Running...`);

  if (process.env.NODE_ENABLED) {
    await producer.connect();

    const response = await fetchRetry(url, {
      retries: 5,
    });
    const data = await response.json();

    const articles = data && data.posts ? data.posts : [];

    for (let x = 0; x < articles.length; x++) {
      const docid = "document/webhose/" + md5(articles[x].url);
      const lang = ISO6391.getCode(articles[x].language);

      console.info(`📩 Sending to ${topics.produce}... ${docid}/${lang}/1`);

      await producer.send({
        topic: topics.produce,
        messages: [
          {
            value: JSON.stringify({
              uuid: docid,

              source: "webhose",
              subsource: articles[x].thread.site,
              mime: "text/plain",
              language: lang,
              published: articles[x].published,
              created: new Date(),
              updated: new Date(),
              pages: 1,

              url: articles[x].url,
              title: articles[x].title || articles[x].thread.title,
              image: articles[x].thread.main_image,

              page: {
                text: articles[x].text,
                page: 1,
                language: lang,
              },

              meta: {
                author: articles[x].author,
                country: articles[x].thread.country,
              },
            }),
          },
        ],
      });
    }

    await producer.disconnect();
  }

  console.info(`🏁 Finished...`);
};

main().catch(console.error);
