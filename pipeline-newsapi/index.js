const pkg = require("./package.json");

const md5 = require("md5");
const dotenv = require("dotenv");
const fetch = require("isomorphic-fetch");
const fetchRetry = require("fetch-retry")(fetch);

const { Kafka } = require("kafkajs");

const env = dotenv.config().parsed;
const topics = {
  produce: process.env.KAFKA_PRODUCE || env.KAFKA_PRODUCE || null,
  consume: process.env.KAFKA_CONSUME || env.KAFKA_CONSUME || null,
};
const langs = (process.env.NODE_LANGUAGES || env.NODE_LANGUAGES || "en").split(
  ","
);
const url =
  "http://newsapi.org/v2/top-headlines?apiKey=558e850437dc4e5e9f9b7494b48d9a9f&language=";

const kafka = new Kafka({
  clientId: pkg.name,
  brokers: [process.env.KAFKA_HOST || env.KAFKA_HOST || "127.0.0.1:9092"],
});

const producer = kafka.producer();

const main = async () => {
  console.info(`🚀 Running...`);

  await producer.connect();

  const log = await Promise.all(
    langs.map(async (lang) => {
      console.info(`🗞️ Fetching... ${url + lang}`);

      const response = await fetchRetry(url + lang, {
        retries: 5,
      });
      const data = await response.json();

      const articles = data && data.articles ? data.articles : [];

      if (articles.length === 0) {
        console.info(`🛑 Failed...`);
      } else {
        for (let x = 0; x < articles.length; x++) {
          const docid = "document/newaspi/" + md5(articles[x].url);

          console.info(`📩 Sending to ${topics.produce}... ${docid}/${lang}/1`);

          await producer.send({
            topic: topics.produce,
            messages: [
              {
                value: JSON.stringify({
                  uuid: docid,

                  source: "newsapi",
                  subsource: articles[x].source.id || articles[x].source.name,
                  mime: "text/plain",
                  language: lang,
                  published: articles[x].publishedAt,
                  created: new Date(),
                  updated: new Date(),
                  pages: 1,

                  url: articles[x].url || "",
                  title: articles[x].title || "",
                  image: articles[x].urlToImage || "",

                  page: {
                    text:
                      articles[x].content ||
                      articles[x].text ||
                      articles[x].description ||
                      "",
                    page: 1,
                    language: lang,
                  },
                }),
              },
            ],
          });
        }

        console.info(`✅ Success...`);
      }
    })
  );

  await producer.disconnect();

  console.info(`🏁 Finished...`);

  return log;
};

main().catch(console.error);
