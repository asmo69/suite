const { gql } = require("apollo-server-express");

module.exports = gql(`
    type Query {
        document(
            id: Int, 
            id: [Int],
            entity_id: Int, 
            entity_id: [Int], 
            all_entity_id: Int, 
            all_entity_id: [Int], 
            uuid: String, 
            language: String, 
            source: String, 
            subsource: String, 
            mime: String, 
            publishedStart: String, 
            publishedEnd: String, 
            createdStart: String, 
            createdEnd: String, 
            updatedStart: String, 
            updatedEnd: String, 
            limit: Int, 
            offset: Int, 
            order: String
        ): [DocumentResult]

        page(
            id: Int, 
            id: [Int],
            document_id: Int, 
            document_id: [Int], 
            language: String, 
            limit: Int, 
            offset: Int, 
            order: String
        ): [PageResult]

        meta(
            id: Int, 
            id: [Int],
            document_id: Int,
            document_id: [Int],
            name: String
            limit: Int, 
            offset: Int, 
            order: String
        ): [MetaResult]

        entity(
            id: Int,
            id: [Int],
            document_id: Int, 
            document_id: [Int], 
            name: String,
            type: String,
            limit: Int, 
            offset: Int, 
            order: String
        ): [EntityResult]

        whoami: [UserResult]

        none: [String]
    }

    type DocumentResult {
        id: Int
        uuid: String
        language: String
        source: String
        subsource: String
        mime: String
        published: String
        created: String
        updated: String
        pages: Int
        url: String
        title: String
        image: String

        page(
            limit: Int, 
            offset: Int, 
            order: String
        ): [PageResult]

        meta(
            limit: Int, 
            offset: Int, 
            order: String
        ): [MetaResult]

        entity(
            limit: Int, 
            offset: Int, 
            order: String
        ): [EntityResult]
    }

    type PageResult {
        id: Int
        document_id: Int
        language: String
        page: Int
        text: String
        markup: String

        document(
            limit: Int, 
            offset: Int, 
            order: String
        ): [DocumentResult]
    }

    type MetaResult {
        id: Int
        document_id: Int
        name: String
        value: String

        document(
            limit: Int, 
            offset: Int, 
            order: String
        ): [DocumentResult]
    }

    type EntityResult {
        id: Int
        name: String
        type: String
        text: String

        document(
            limit: Int, 
            offset: Int, 
            order: String
        ): [DocumentResult]
    }

    type UserResult {
        email: String
        name: String
        organisation: String
        groups: [String]
    }
`);
