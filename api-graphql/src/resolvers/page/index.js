const qs = require("query-string");
const https = require("https");
const dotenv = require("dotenv");

const fetch = require("isomorphic-fetch");
const fetchRetry = require("fetch-retry")(fetch);

const document = require("../document");

const env = dotenv.config().parsed;
const config = {
  ...env,
  ...process.env,
};

module.exports = (args, context) => {
  const url =
    "https://" +
    (config.NODE_DOCUMENT_HOST || "localhost") +
    "/page?" +
    qs.stringify(args);
  return fetchRetry(url, {
    retries: 5,
    headers: {
      Authorization: context.authorization,
      Cookie: context.cookie,
    },
    agent:
      url.substr(0, 5) === "https"
        ? new https.Agent({
            rejectUnauthorized: false,
          })
        : null,
  })
    .then((response) => response.json())
    .then((response) => response.rows || [])
    .then((rows) =>
      rows.map((row) => {
        row.document = (args) =>
          document(
            {
              ...args,
              id: row.document_id,
            },
            context
          );

        return row;
      })
    );
};
