const document = require("./document");
const page = require("./page");
const meta = require("./meta");
const entity = require("./entity");
const whoami = require("./whoami");
const none = require("./none");

module.exports = {
  Query: {
    document: (parent, args, context) => document(args, context),
    page: (parent, args, context) => page(args, context),
    meta: (parent, args, context) => meta(args, context),
    entity: (parent, args, context) => entity(args, context),
    whoami: (parent, args, context) => whoami(args, context),
    none: (parent, args, context) => none(args, context),
  },
};
